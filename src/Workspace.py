#!/usr/bin/env python
# coding: utf-8

# In[230]:


import matplotlib.pyplot as plt
import numpy as np
import csv
import scipy.integrate as integ
import sympy as sp
import sympy.plotting as splot
import pandas as pd

from scipy import misc
from scipy.spatial import ConvexHull

from numpy import cos,sin,pi, arccos
from numpy.linalg import inv

from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

from sympy.sets import Interval

from sklearn import datasets

from sympy import solveset, symbols, Interval, Min



from spatialmath import *
from roboticstoolbox import *

panda = models.DH.Panda()
q_neutral=(panda.qlim[0]+panda.qlim[1])/2
print(panda)
print("q_neutral = ",q_neutral)
panda.plot(q_neutral,block=False)



ft=24 ; #Police size


# In[3]:


n_path=13; # path number in "TrajectoryCharacteristics.ods" file
# VARIABLES
nb_points = 150 ;

#A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*s**2 + I*s**3
A=0;         #        #0
B=1;         # sin    #0
C=0;         # cos    #3/2
D=0;         # sin^2  #0
E=0;         # cos^2  #2
F=0;         # sin^3  #0
G=0;         # cos^3  #0
H=0;         # x^2
I=0          # x^3

f1=3*sp.pi;    #6*sp.pi # Here is not frequency but pulsation, it should be written omega_1
f11=0;    #6*sp.pi
f12=0;    #6*sp.pi
f2=0;        #2*sp.pi
f3=0;        #0

dA=0;
dH=0;
dI=0;

X_start=0.5;
Y_start=-0.2;
Z_start=0.5;

depth=0; # trajectory depth in real space X
width=0.4; # trajectory width in real space Y
height=0.1 # trajectory height in real space Z

# Curvilinear abscissa
s = sp.Symbol('s') 
s_eval=np.arange(0,1,1/nb_points);
s_max = 1
s_min = 0
S_Interval=Interval(s_min,s_max)

# THEORETICAL FUNCTIONS
def X_t(s):
    return 1e-5*s
    #return 1e-5*s+A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
    #return A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
def Y_t(s):
    return s
def Z_t(s):
    #return 2*s
    return A*(dA+s)+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*(dH+s)**2 + I*(dI+s)**3;
    #return A*s+B*sp.atan(sp.atan(sp.cos(f11*s)))+C*sp.atan(sp.atan(sp.cos(f12*s)))+D*sp.atan(sp.atan(sp.sin(f2*s)))**2+E*sp.atan(sp.atan(sp.cos(f2*s)))**2+F*sp.atan(sp.atan(sp.sin(f3*s)))**3+G*sp.atan(sp.atan(sp.cos(f3*s)))**3;

# COMPUTING
splot.plot_parametric((Y_t(s),Z_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='z_t(s)', title='Theoretical path - Z(s) =f(Y(s))')
splot.plot_parametric((Y_t(s),X_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='x_t(s)', title='Theoretical path - X(s) =f(Y(s))')
splot.plot3d_parametric_line(X_t(s),Y_t(s),Z_t(s),(s,s_min,s_max),xlabel="x_t(s)",ylabel='y_t(s)',zlabel='z_t(s)', title='3D theoretical path')
print("x_t(s) = ",X_t(s))
print("y_t(s) = ",Y_t(s))
print("z_t(s) = ",Z_t(s))

# Computing Max Min of theoretical functions
X_t_max = sp.maximum(X_t(s), s, S_Interval)
print('X_t_max =\t',X_t_max)
X_t_min = sp.minimum(X_t(s), s, S_Interval)
print('X_t_min =\t',X_t_min)
Y_t_max = sp.maximum(Y_t(s), s, S_Interval)
print('Y_t_max =\t',Y_t_max)
Y_t_min = sp.minimum(Y_t(s), s, S_Interval)
print('Y_t_min =\t',Y_t_min)
Z_t_max = 1 #sp.maximum(Z_t(s), s, S_Interval) # too complicated to compute
print('Z_t_max =\t',Z_t_max)
Z_t_min = -1 #sp.minimum(Z_t(s), s, S_Interval) # too complicated to compute
print('Z_t_min =\t',Z_t_min)

# DERIVATIVE
dx_t=sp.diff(X_t(s),s)
dy_t=sp.diff(Y_t(s),s)
dz_t=sp.diff(Z_t(s),s)

splot.plot(dz_t,(s,0,1),xlabel="s",ylabel='dz_t(s)', title='Derived theoretical path - dZ(s) =f(s)')
print("\ndx_t(s)/ds = ",dx_t)
print("dy_t(s)/ds = ",dy_t)
print("dz_t(s)/ds = ",dz_t)


# In[52]:


# REAL FUNCTIONS
def X_r(s):
    return (X_t(s)-X_t(0)) * depth / (X_t_max-X_t_min) + X_start +1e-10*s
def Y_r(s):
    return (Y_t(s)-Y_t(0)) * width / (Y_t_max-Y_t_min) + Y_start
def Z_r(s):
    return (Z_t(s)-Z_t(0)) * height / (Z_t_max-Z_t_min) + Z_start


# COMPUTING

plot_size = max(depth,width,height)
splot.plot(X_r(s),(s,s_min,s_max),xlabel="s",ylabel='x_r(s)', title='Real path - X =f(s)')
splot.plot(Y_r(s),(s,s_min,s_max),xlabel="s",ylabel='y_r(s)', title='Real path - Y =f(s)')
splot.plot(Z_r(s),(s,s_min,s_max),xlabel="s",ylabel='z_r(s)', title='Real path - Z =f(s)')
splot.plot(Z_r(s),(s,0.5,0.7),xlabel="s",ylabel='z_r(s)', title='Real path - Z =f(s)')
splot.plot_parametric((Y_r(s),Z_r(s)),(s,s_min,s_max),xlabel="y_r(s)",ylabel='z_r(s)', title='Real path - Z(s) =f(Y(s))')
splot.plot3d_parametric_line(X_r(s),Y_r(s),Z_r(s),(s,s_min,s_max),xlim=[X_start+depth/2-plot_size/2,X_start+depth/2+plot_size/2],ylim=[Y_start+width/2-plot_size/2,Y_start+width/2+plot_size/2],zlim=[0.1,0.9],xlabel="x_r(s)",ylabel='y_r(s)',zlabel='z_r(s)', title='3D real path')
print("x_r(s) = ",X_r(s))
print("y_r(s) = ",Y_r(s))
print("z_r(s) = ",Z_r(s))

x_r=X_r(s)
y_r=Y_r(s)
z_r=Z_r(s)

# DERIVATIVE
dx_r=sp.diff(X_r(s),s)
dy_r=sp.diff(Y_r(s),s)
dz_r=sp.diff(Z_r(s),s)

splot.plot(dz_r,(s,0,1),xlabel="s",ylabel='dz_r(s)', title='Derived real path - dZ =f(s)')
print("\ndx_r(s)/ds = ",dx_r)
print("dy_r(s)/ds = ",dy_r)
print("dz_r(s)/ds = ",dz_r)

# DOUBLE DERIVATIVE
ddx_r=sp.diff(dx_r,s)
ddy_r=sp.diff(dy_r,s)
ddz_r=sp.diff(dz_r,s)

splot.plot(ddz_r,(s,0,1),xlabel="s",ylabel='ddz_r(s)', title='2nd Derived real path - ddZ =f(s)')
print("\nddx_r(s)/dds = ",ddx_r)
print("ddy_r(s)/dds = ",ddy_r)
print("ddz_r(s)/dds = ",ddz_r)

# Curvature

# Angle
alpha_x_r=sp.atan(dx_r)
alpha_y_r=sp.atan(dy_r)
alpha_z_r=sp.atan(dz_r)

splot.plot(alpha_z_r,(s,0,1),xlabel="s",ylabel='alpha_z_r(s)', title='Orientation along the path - alpha =f(s)')
print("\nalpha_x_r(s) = ",alpha_x_r)
print("alpha_y_r(s) = ",alpha_y_r)
print("alpha_z_r(s) = ",alpha_z_r)


# Angle Derivative
dalpha_x_r=sp.diff(alpha_x_r,s)
dalpha_y_r=sp.diff(alpha_y_r,s)
dalpha_z_r=sp.diff(alpha_z_r,s)

splot.plot(dalpha_z_r,(s,0,1),xlabel="s",ylabel='dalpha_z_r(s)', title='Derived orientation along the path - dalpha =f(s)')
print("\ndalpha_x_r(s) = ",dalpha_x_r)
print("dalpha_y_r(s) = ",dalpha_y_r)
print("dalpha_z_r(s) = ",dalpha_z_r)


# # Compute frame on each point of the curve

# In[5]:


def PathFrame(s_test):
    #print("s test = ",s_test)

    # TANGENT VECTOR
    r12 = -dx_r.evalf(subs={s:s_test})
    r22 = -dy_r.evalf(subs={s:s_test})
    r32 = -dz_r.evalf(subs={s:s_test})
    #r12 = -dx_r.evalf(abscissa_test)
    #r22 = -dy_r.evalf(abscissa_test)
    #r32 = -dz_r.evalf(abscissa_test)

    normT=sp.sqrt(r12**2+r22**2+r32**2)
    r12 = r12/normT
    r22 = r22/normT
    r32 = r32/normT

    #print("T = \n\t",r12,"\n\t",r22,"\n\t",r32)

    # NORMAL VECTOR
    r13 = 0 # on the plane Oy_0z_0, orthogonal to x_0 vector
    if(r32==0):
        r23=0
        r33=1
    else:
        r23 = sp.sqrt(1/((r22/r32)**2+1))
        r33 = -r23 * r22 / r32

    if (r33>0):
        r13=-r13
        r23=-r23
        r33=-r33

    #r23 = sp.sqrt(1/((r22/r12)**2+1))
    #r13 = -r23 * r22 / r12  # on the plane Oy_0x_0, orthogonal to x_0 vector
    #r33 = 0 # on the plane Oy_0x_0, orthogonal to x_0 vector
    #if (r13<0):
    #    r13=-r13
    #    r23=-r23
    #    r33=-r33

    #print("\nN = \n\t",r13,"\n\t",r23,"\n\t",r33)


    # 2nd NORMAL VECTOR
    r11 = r22*r33 - r32*r23
    r21 = r32*r13 - r12*r33
    r31 = r12*r23 - r22*r13
    #print("\nN2 = \n\t",r11,"\n\t",r21,"\n\t",r31)

    # Vectors norms
    #print("\nT norm = \t", sp.sqrt(r12**2+r22**2+r32**2))
    #print("N norm = \t", sp.sqrt(r13**2+r23**2+r33**2))
    #print("N2 norm = \t", sp.sqrt(r11**2+r21**2+r31**2))

    R=[[r11, r12, r13],[r21, r22, r23],[r31, r32, r33]]
    #print("\nR = \n", R[0],"\n", R[1],"\n", R[2])

    detR=R[0][1]*R[1][2]*R[2][0]-R[0][1]*R[2][2]*R[1][0]+R[1][1]*R[2][2]*R[0][0]-R[1][1]*R[0][2]*R[2][0]+R[2][1]*R[0][2]*R[1][0]-R[2][1]*R[1][2]*R[0][0]
    #print("\ndet(R) = ", detR)

    p=[X_r(s_test),Y_r(s_test),Z_r(s_test)]
    #print("\np = \n",p)
    return np.array([R[0]+[p[0]],R[1]+[p[1]],R[2]+[p[2]],[0.0,0.0,0.0,1.0]]).astype(np.float)


# ### Transform into Spacial Math Package function (example with s=0)

# In[6]:


#T_goal=SE3(PathFrame(0),check=False)
#T_goal


# ### Reference abscissa

# In[7]:


nb_points_s=40
s_pas=1/(nb_points_s-1)
s_list = np.arange(0,1+s_pas,s_pas)
s_list


# ### Rotation around wireloop

# In[8]:


nb_points_alpha=40
alpha_min=-np.pi
alpha_max=(nb_points_alpha/2-1)*np.pi/(nb_points_alpha/2)
alpha_pas=(alpha_max-alpha_min)/(nb_points_alpha-1)
alpha_list = np.arange(alpha_min,alpha_max+alpha_pas,alpha_pas)
alpha_list


# In[9]:


alpha_list[int(len(alpha_list)/2)]


# ### Solve IK

# In[10]:


#q_ini=np.array([ 0.00101961,-0.78475,-0.000276699,-2.35668,-0.00178532,1.57149,0.784751])
#sol=panda.ikine_LM(T_test,q0=q_ini)


# ### Define functions q solving IK

# In[11]:


def q(s,alpha,q0):
    T_goal=SE3(PathFrame(s),check=False)
    T_alpha = SE3.Ry(alpha)
    T_test = T_goal * T_alpha
    sol=panda.ikine_LM(T_test,q0=q0)
    #print(T_test)
    return sol

def q_frame(T_test,q0):
    sol=panda.ikine_LM(T_test,q0=q0)
    return sol


# ### Compute solutions
# 

# In[12]:



print('\nSolving IK for the robot along the wireloop for different orientations around it')
print('Computed with:')
print('s = [',s_list[0],'; ',s_list[-1],'] in ', s_list.size,' points')
print('alpha = [',alpha_list[0],'; ',alpha_list[-1],'] in ', alpha_list.size,' points')
q_ini=np.array([ 0.00101961,-0.78475,-0.000276699,-2.35668,-0.00178532,1.57149,0.784751]) # INITIAL GUESS
sol_tab=np.zeros((len(s_list),len(alpha_list),7))
sol_alpha_tab=[]
no_sol=[]
for j in range(len(s_list)):
    for k in range(len(alpha_list)):
        sj=s_list[j]
        alphak=alpha_list[k]
        q_sol=q(sj,alphak,q_ini)[0]
        k_rec=k
        j_rec=j
        while((q_sol<panda.qlim[0]).any() or (q_sol>panda.qlim[1]).any()):
            print("Solutions out of bounds")
            if (k_rec-1>=0):
                q_ini=sol_tab[j_rec][k_rec-1]
                k_rec=k_rec-1
            elif (j_rec-1>=0):
                q_ini=sol_tab[j_rec-1][k_rec]
                j_rec=j_rec-1
            else:
                print("Pas de solution disponible, s=",sj,", alpha=",alphak)
                no_sol.append([sj,alphak])
                break
            q_sol=q(sj,alphak,q_ini)[0]
        q_ini=q_sol
        #sol_alpha_tab.append(q_sol)
        sol_tab[j][k]=q_sol
    #sol_tab.append(sol_alpha_tab)


# In[37]:


sol_tab_save


# ### Save solutions in csv file

# In[34]:


tab_export_q_sol=np.zeros((len(s_list)*len(alpha_list),9))

ind=0
for j in range(len(s_list)):
    for k in range(len(alpha_list)):
        sj=s_list[j]
        alphak=alpha_list[k]
        tab_export_q_sol[ind][0]=sj
        tab_export_q_sol[ind][1]=alphak
        for col in range(7):
            tab_export_q_sol[ind][2+col]=sol_tab[j][k][col]        
        ind+=1
np.savetxt("q_sol_13.csv",tab_export_q_sol,delimiter=",",header="s,alpha,q1,q2,q3,q4,q5,q6,q7",comments="")


# ### Plot solutions

# In[14]:



print('\nPlotting solution for articular space resolution')

fig=plt.figure(figsize=(20,20))

for qi in range(1,8):
    ax = fig.add_subplot(3,3,qi, projection='3d')
    ax.set_xlim(auto=True)
    ax.set_xlabel("s")
    ax.set_ylim(auto=True)
    ax.set_ylabel("alpha (rad)")
    ax.set_zlim(auto=True)
    ax.set_zlabel("q (rad)")
    ax.set_title("q"+str(qi))
    for j in range(s_list.size):
        for k in range(alpha_list.size):
            ax.scatter(s_list[j],alpha_list[k],sol_tab[j][k][qi-1],color='blue')
plt.show()


# ### Verify with FK

# In[188]:


print('\nPlotting Trajectory depending on solutions found')

fig=plt.figure(figsize=(20,20))

ax = plt.axes(projection='3d')
ax.set_xlim([0,1])
ax.set_xlabel("X (m)")
ax.set_ylim(auto=True)
ax.set_ylabel("Y (m)")
ax.set_zlim(auto=True)
ax.set_zlabel("Z (m)")
ax.set_title("Forward Kinematics reconstruction")
for i in range(s_list.size):
    q_test=sol_tab[i][int(len(alpha_list)/2)]
    X_test=panda.fkine(q_test).t
    s_test=s_list[i]
    ax.scatter(X_test[0],X_test[1],X_test[2])
plt.show()


# In[179]:


X_test[2]


# ### Study Null-Space

# In[13]:


#J0 = panda.jacob0(sol_tab[0][0])
#J0inv=np.linalg.pinv(J0)
#Id7=np.eye(J0inv.shape[0])
#null_space_matrix = Id7 - J0inv.dot(J0)


# In[14]:


#q_dot_null = np.array([1,1,1,0,1,1,1])


# In[15]:


#null_space_matrix.dot(q_dot_null)


# ### Study Jacobian singularities (rank of the matrix)

# In[16]:


#u_J0, d_J0, v_J0 = np.linalg.svd(J0)


# In[17]:


#rank_J0 = np.linalg.matrix_rank(J0)
#if rank_J0 == 6:
#   print("J0 is full rank matrix")


# In[18]:



print('\nCompute Jacobian Singularities around the computed Wireloop')

d_J0_tab=[]
d_J0_alpha_tab=[]
rank_J0_tab=[]
rank_J0_alpha_tab=[]
for j in range(s_list.size):
        for k in range(alpha_list.size):
            J0 = panda.jacob0(sol_tab[j][k])
            u_J0, d_J0, v_J0 = np.linalg.svd(J0)
            rank_J0 = np.linalg.matrix_rank(J0)
            #print(rank_J0)
            if rank_J0 != 6:
                print("\tJacobian is degenerated for result with: \ts = ",s_list[j], ",\talpha = ",alpha_list[k])
            d_J0_alpha_tab.append(d_J0)
            rank_J0_alpha_tab.append(rank_J0)
            
        rank_J0_tab.append(rank_J0_alpha_tab)
        d_J0_tab.append(d_J0_alpha_tab)


# # Robot Cartesian Workspace

# ### From IK with cartesian space sampling

# In[217]:


working_cube=np.array([[0,1],[0,1],[0,1]]) # [[xmin,xmas],[ymin,ymax],[zmin,zmax]]
nb_points_cart=[10,10,10] #[nb_pts X,nb_pts Y, nb_pts Z]
nb_points_cart_or=[4,1,1]
R_a=SE3.Ry(np.pi/2)
R_b=SE3.Ry(-np.pi/2)
R_c=SE3.Rz(np.pi/2)
R_d=SE3.Rz(-np.pi/2)
R_e=SE3.Rz(np.pi)
R_f=SE3.Rx(0)
R_tab=[R_a,R_b,R_c,R_d,R_e,R_f]

nb_tests=nb_points_cart[0]*nb_points_cart[1]*nb_points_cart[2]*len(R_tab)

print("Nombre de tests: ",nb_tests)

#IK_cart_sol=np.array((nb_tests,7))
IK_cart_sol=[]

# TO IMPROVE !
q_ini=np.array([ 0.00101961,-0.78475,-0.000276699,-2.35668,-0.00178532,1.57149,0.784751]) # INITIAL GUESS 

ind=0
for x_i in range(nb_points_cart[0]):
    for y_i in range(nb_points_cart[1]):
        for z_i in range(nb_points_cart[2]):
            for r_i in R_tab:
                percentage=(ind/nb_tests*100)
                print(f'Plotting Points: {percentage:.2f} %', end='\r')
                ind+=1
                #print("x_i =" ,x_i)
                #print("y_i =" ,y_i)
                #print("z_i =" ,z_i)
                #print("ro_i =" ,ro_i)
                #print("pi_i =" ,pi_i)
                #print("ya_i =" ,ya_i)
                x_test=(working_cube[0][1]-working_cube[0][0])/nb_points_cart[0]*x_i+working_cube[0][0]
                y_test=(working_cube[1][1]-working_cube[1][0])/nb_points_cart[1]*y_i+working_cube[1][0]
                z_test=(working_cube[2][1]-working_cube[2][0])/nb_points_cart[2]*z_i+working_cube[2][0]
                #print("V_test =" ,[x_test,y_test,z_test])
                frame_test=SE3([x_test,y_test,z_test])*r_i#*SE3.Rz(ya_i)*SE3.Ry(pi_i)*SE3.Rx(ro_i)
                sol=q_frame(frame_test,q_ini)
                #IK_cart_sol.append(sol[0])
                        


# In[ ]:


x_test=
frame_test=SE3([x_test,y_test,z_test])*r_i#*SE3.Rz(ya_i)*SE3.Ry(pi_i)*SE3.Rx(ro_i)
sol=q_frame(frame_test,q_ini)


# In[223]:


panda.fkine(q_neutral)


# In[229]:


panda.plot(q_neutral,block=False)


# ### From FK with articular space sampling

# In[19]:


cut = 5
print('\nCompute Robot Workspace')

P_tab=[]
cut_tab=[cut,cut,cut,cut,cut,cut,cut]
q_min=panda.qlim[0]
q_max=panda.qlim[1]
delta_q=q_max-q_min
for i1 in range(cut_tab[0]+1):
    q1 = q_min[0] + i1*delta_q[0]/cut_tab[0]
    #print(q1)
    for i2 in range(cut_tab[1]+1):
        q2 = q_min[1] + i2*delta_q[1]/cut_tab[1]
        for i3 in range(cut_tab[2]+1):
            q3 = q_min[2] + i3*delta_q[2]/cut_tab[2]
            for i4 in range(cut_tab[3]+1):
                q4 = q_min[3] + i4*delta_q[3]/cut_tab[3]
                for i5 in range(cut_tab[4]+1):
                    q5 = q_min[4] + i5*delta_q[4]/cut_tab[4]
                    for i6 in range(cut_tab[5]+1):
                        q6 = q_min[5] + i6*delta_q[5]/cut_tab[5]
                        for i7 in range(cut_tab[6]+1):
                            q7 = q_min[6] + i7*delta_q[6]/cut_tab[6]
                            q_test=[q1,q2,q3,q4,q5,q6,q7]
                            P_tab.append(panda.fkine(q_test).t)
print("Nombre de points: ",len(P_tab))


# In[189]:


panda.qlim


# In[196]:


test=np.array([0,0,0,0,0,0,0])
if(test>panda.qlim[0]).any():
    print("che")


# ### Compute Convex Hull

# In[20]:


P_array=np.array(P_tab)
hull = ConvexHull(P_array)


# ### Plot Workspace and convex hull

# In[24]:


fig_cartesian=plt.figure(figsize=(40,40))

ax_cartesian = plt.axes(projection ='3d')
ax_cartesian.set_xlim(auto=True)
ax_cartesian.set_ylim(auto=True)
ax_cartesian.set_zlim(auto=True)
ax_cartesian.set_xlabel("X (m)")
ax_cartesian.set_ylabel("Y (m)")
ax_cartesian.set_zlabel("Z (m)")
ax_cartesian.set_title('Robot Workspace')

# Plot points
for p_i in range(len(P_tab)):
    percentage=(p_i/len(P_tab)*100)
    print(f'Plotting Points: {percentage:.2f} %', end='\r')
    ax_cartesian.scatter(P_tab[p_i][0],P_tab[p_i][1],P_tab[p_i][2],color='red')

print('\nPoints plotted')

# Compute Convex Hull
print('Computing Convex Hull')
P_array=np.array(P_tab)
hull = ConvexHull(P_array)
print('Plotting Convex Hull')
for simplex in hull.simplices:
    ax_cartesian.plot3D(P_array[simplex, 0], P_array[simplex, 1], P_array[simplex, 2], 'k-')

print('Convex Hull plotted')
print('\nShowing results')
plt.show()


# ### Place Wireloop in the convex hull

# In[ ]:


fig_convexhull=plt.figure(figsize=(40,40))
ax_convexhull = plt.axes(projection ='3d')

for simplex in hull.simplices:
    ax_convexhull.plot3D(P_array[simplex, 0], P_array[simplex, 1], P_array[simplex, 2], 'k-')
for s_i in s_list:
    ax_convexhull.scatter(X_r(s_i),Y_r(s_i),Z_r(s_i), color='blue')
    
    

plt.show()


# ### Check if some points are in the convex hull

# In[ ]:


import numpy as np

import matplotlib.pyplot as plt

from scipy.spatial import Delaunay, delaunay_plot_2d, tsearch

rng = np.random.default_rng()

pts = rng.random((20, 2))

tri = Delaunay(pts)
_ = delaunay_plot_2d(tri)

loc = rng.uniform(0.2, 0.8, (5, 2))

s = tsearch(tri, loc)
print(pts)
print( tri.simplices)
print(s)
plt.triplot(pts[:, 0], pts[:, 1], tri.simplices[s], 'b-', mask=s==-1)

plt.scatter(loc[:, 0], loc[:, 1], c='r', marker='x')

plt.show()


# In[ ]:



import numpy as np

import matplotlib.pyplot as plt

from scipy.spatial import Delaunay, delaunay_plot_2d, tsearch
fig=plt.figure(figsize=(10,10))
ax= plt.axes(projection ='3d')

rng = np.random.default_rng()

pts = rng.random((20, 3))
hull = ConvexHull(pts)
pts_cvx_list=[]
for i in range (hull.simplices.shape[0]):
    pts_cvx_list.append([pts[hull.simplices[i][0],0], pts[hull.simplices[i][1],1], pts[hull.simplices[i][2],2]])
pts_cvx = np.array(pts_cvx_list)
tri = Delaunay(pts_cvx)

loc = rng.uniform(0.2, 0.8, (5, 3))

s = tsearch(tri, loc)
#print('pts:\n',pts)
#print('pts 0 :\n',pts[0])
#print( 'tri.simplices:\n',tri.simplices)
#print( 'tri.simplices S:\n',tri.simplices[s])
#print('s:\n',s)
#plt.triplot(pts[:, 0], pts[:, 1], tri.simplices[s], 'b-', mask=s==-1)

#plt.scatter(loc[:, 0], loc[:, 1], c='r', marker='x')

#plt.show()


print(tri.simplices)
for simplex in tri.simplices:
#simplex=tri.simplices[0]
    #print(simplex)
    ax.plot3D(pts_cvx[simplex,0],pts_cvx[simplex,1],pts_cvx[simplex,2], 'g-')
for simplex in tri.simplices[s]:
#simplex=tri.simplices[0]
#print(simplex)
    ax.plot3D(pts_cvx[simplex,0],pts_cvx[simplex,1],pts_cvx[simplex,2], 'b-')
for point in loc:
    ax.scatter(point[0], point[1],point[2], 'r-')


# In[ ]:


import numpy as np

import matplotlib.pyplot as plt

from scipy.spatial import Delaunay, delaunay_plot_2d, tsearch
fig=plt.figure(figsize=(10,10))
ax= plt.axes(projection ='3d')

rng = np.random.default_rng()

pts = rng.random((20, 3))
hull = ConvexHull(pts)

tri = Delaunay(hull.simplices)
#_ = delaunay_plot_2d(tri)

loc = rng.uniform(0.2, 0.8, (5, 3))

s = tsearch(tri, loc)
print('pts:\n',pts)
print('pts 0 :\n',pts[0])
print( 'tri.simplices:\n',tri.simplices)
print( 'tri.simplices S:\n',tri.simplices[s])
print('s:\n',s)
#plt.triplot(pts[:, 0], pts[:, 1], tri.simplices[s], 'b-', mask=s==-1)

#plt.scatter(loc[:, 0], loc[:, 1], c='r', marker='x')

#plt.show()

results = tsearch(tri, pts)
shapes=tri.simplices

for simplex in tri.simplices:
#simplex=tri.simplices[0]
#print(simplex)
    ax.plot3D(pts[simplex,0],pts[simplex,1],pts[simplex,2], 'g-')
for simplex in tri.simplices[s]:
#simplex=tri.simplices[0]
#print(simplex)
    ax.plot3D(pts[simplex,0],pts[simplex,1],pts[simplex,2], 'b-')
for point in loc:
    ax.scatter(point[0], point[1],point[2], 'r-')


# In[ ]:


import numpy as np

import matplotlib.pyplot as plt

from scipy.spatial import Delaunay, delaunay_plot_2d, tsearch

rng = np.random.default_rng()

pts = rng.random((20, 2))
pts = np.array([[float(X_r(0)), float(Y_r(0)), float(Z_r(0))],[float(X_r(1)), float(Y_r(1)), float(Z_r(1))],[float(X_r(0.3)) ,float(Y_r(0.3)) ,float(Z_r(0.3))],[float(X_r(0.6) ),float(Y_r(0.6)) ,float(Z_r(0.6))],[float(X_r(0.8)),float(Y_r(0.8)) ,float(Z_r(0.8))],[float(X_r(0.15)) ,float(Y_r(0.15)) ,float(Z_r(0.15))]])
print(pts)
tri = Delaunay(P_array)

fig_convexhull=plt.figure(figsize=(10,10))
ax_convexhull = plt.axes(projection ='3d')

results = tsearch(tri, pts)
shapes=tri.simplices
print(tri.simplices)
print(s)
#for simplex in tri.simplices:
    #ax_convexhull.plot3D(P_array[simplex, 0], P_array[simplex, 1], P_array[simplex, 2], 'k-')


# In[ ]:


tri.simplices


# ### Display in color

# from colour import Color
# c1 = Color("green")
# 
# c2 = Color("red")
# 
# gradient = list(c1.range_to(c2, 100))
# 
# grad_rgb = [x.rgb for x in gradient]
# 
# grad_rgb_255 = [list(map(lambda x: int(x*255), i)) for i in grad_rgb]
# 
# palette = np.array(grad_rgb_255)
# 
# 
# ind = np.linspace(0,99,100, dtype=int).reshape(1,100)
# 
# fig = plt.figure(figsize=(8,2))
# 
# ax = fig.add_subplot(111)
# 
# ax.imshow(palette[ind])
# 
# ax.axis("off")
# 
# plt.show()

# ### Solve optimization function to find best path

# In[129]:


q1_ = sp.Symbol('q1') 
q2_ = sp.Symbol('q2')
q3_ = sp.Symbol('q3')
q4_ = sp.Symbol('q4')
q5_ = sp.Symbol('q5')
q6_ = sp.Symbol('q6')
q7_ = sp.Symbol('q7')
q_=[q1_,q2_,q3_,q4_,q5_,q6_,q7_]

ry_test=SE3(0.2074,0.1934,0.1747)*SE3.Ry(0.5)
print(ry_test)
q_test=panda.ikine_LM(ry_test,q0=(panda.qlim[0]+panda.qlim[1])/2)
print(q_test)
test=panda.fkine(q_test)
print(test)
print(q_test>panda.qlim[0])
print(q_test<panda.qlim[1])


# In[125]:


Xtest=panda.fkine(q_test)
print(Xtest)
qsol=panda.ikine_LM(Xtest,q0=(panda.qlim[0]+panda.qlim[1])/2)[0]

print(qsol)
Xsol=panda.fkine(qsol)

print(Xsol)


# In[209]:


def MGD_(q_):
    return panda.fkine(q_)
def X_(q_):
    return panda.fkine(q_).t[0]
def Y_(q_):
    return panda.fkine(q_).t[1]
def Z_(q_):
    return panda.fkine(q_).t[2]
def Theta_y_(q_):
    return sp.asin(-MGD_(q_).R[2,0])

def MGD(q):
    return panda.fkine(q)
def X(q):
    return panda.fkine(q).t[0]
def Y(q):
    return panda.fkine(q).t[1]
def Z(q_):
    return panda.fkine(q).t[2]
def Theta_y(q):
    r31=MGD(q).R[2,0]
    if(r31>1):
        r31=1
    elif(r31<-1):
        r31=-1
    return np.arcsin(-r31)
def oppTheta_y(q):
    return -Theta_y(q)


# In[210]:




from scipy.optimize import least_squares,minimize,Bounds

q_init=(panda.qlim[0]+panda.qlim[1])/2
#q_init=0
print(q_init)
lb=panda.qlim[0]
ub=panda.qlim[1]
print(lb)
print(ub)
q_theta_y_min = minimize(Theta_y, q_init,bounds=Bounds(lb,ub)) 
q_theta_y_max = minimize(oppTheta_y, q_init)
print("\nq_theta_y_min= \n",q_theta_y_min.x)
print((q_theta_y_min.x>lb).any())
print((q_theta_y_min.x<ub).any())

print("MGD:\n",MGD(q_theta_y_min.x))
print("\nq_theta_y_max= \n",q_theta_y_max.x)
print((q_theta_y_max.x>lb).any())
print((q_theta_y_max.x<ub).any())

#theta_y_min=np.arcsin(-MGD_(q_theta_y_min.x).R[2,0])
#theta_y_max=np.arcsin(-MGD_(q_theta_y_max.x).R[2,0])
theta_y_min=Theta_y(q_theta_y_min.x)
theta_y_max=Theta_y(q_theta_y_max.x)

print("\ntheta_y_min= \n",theta_y_min)
print("\ntheta_y_max= \n",theta_y_max)

