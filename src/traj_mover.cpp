#include <traj_mover/traj_mover.h>

void TrajMover::Init(traj_mover::MoverProperties& mover_properties_)
{
    mover_properties_.follower_=false;
    mover_properties_.follow_init_=false;
    tf::poseMsgToKDL(mover_properties_.oMroot_,oMroot_kdl);
    oProot_kdl = oMroot_kdl.p;
}

void TrajMover::BuildInitFollow(kdl_trajectories::TrajProperties &traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 X_curr)
{
    // Variables
    double vel=0.1;
    KDL::Frame start_frame;
    bool verbose = false;

    // Updating variables
    start_frame=PathFrame(mover_properties_,0);
    std::cout << "Start frame =\n" << start_frame << std::endl;
    //double roll_test, pitch_test, yaw_test;
    //start_frame.M.GetRPY(roll_test, pitch_test, yaw_test);
    //cout << "Start orientation \tR=" << roll_test << "\tP=" << pitch_test << "\tY=" << yaw_test << endl;

    // Building trajectory elements
    movement_list.clear();

    MovementElement list_element;
    list_element.movetype = "MOVEL";
    list_element.options.push_back(vel);
    list_element.frame = start_frame;
    movement_list.push_back(list_element);
    ROS_INFO_STREAM("Follower Mode: Building trajectory to start frame on the path");
    // Building trajectory
    
    Eigen::Affine3d x_curr(X_curr.toHomogeneousMatrix());
    Build(x_curr, verbose); 
}

void TrajMover::ReachInitFrame(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 oMtip)
{

     //// Computing distance from Current Frame to Start Frame
    double err_trans, err_rot;
    Eigen::Affine3d start_frame;
    tf::transformKDLToEigen(PathFrame(mover_properties_,0),start_frame);

    // Distance from Start Frame
    const pinocchio::SE3 oMdes(start_frame.matrix());
    const pinocchio::SE3 tipMdes = oMtip.actInv(oMdes);
    double dist_trans = tipMdes.translation().norm();
    //std::cout << "dist_trans = " << dist_trans << std::endl;
    double dist_rot = tipMdes.rotation().norm();
    //std::cout << "dist_rot = " << dist_rot << std::endl;
    //std::cout << "rot = " << tipMdes.rotation() << std::endl;

    if (dist_trans < 1e-2)// TODO ADD ROTATION COMPARISON && dist_rot < 1e-2) // Hardcoded threshold error of 1 cm -> set to 1e-2
    {   
        ROS_WARN_STREAM("INITIAL POSE REACHED");
        mover_properties_.follow_init_=1;       
        publishTrajectory();
    }
    

}



void TrajMover::updatePose(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, double time_dt)
{
    tf::poseMsgToKDL(traj_properties_.X_curr_, X_curr_);
    tf::twistMsgToKDL(mover_properties_.Xd_curr_, Xd_curr_);
    if (mover_properties_.follower_ && !mover_properties_.follow_init_)
    {
        t_traj_ += time_dt;
        if (t_traj_ > ctraject->Duration() && in_loop)
        {
            t_traj_ = init_dur;
        }
        X_traj_ = ctraject->Pos(t_traj_);
        Xd_traj_ = ctraject->Vel(t_traj_);
        Xdd_traj_ = ctraject->Acc(t_traj_);
    }
    else if (mover_properties_.follow_init_)
    {
        mover_properties_.s_next_ = FollowSampledPath(traj_properties_, mover_properties_,X_curr_,Xd_curr_); 
        X_traj_ = PathFrame(mover_properties_,mover_properties_.s_next_);
        Xd_traj_.Zero();
        Xdd_traj_.Zero();
      /*
      if ( (presampler_initialized_ == true )){
        s_next_ = FollowSampledPath(traj_properties_,X_curr_,Xd_curr_); // See below for  traj_properties_.s_next_ = s_next_;
        mover_properties_.s_next_ = s_next_;
        X_traj_ = PathFrame(s_next_);
      }
      else {
        ROS_ERROR_STREAM("CONTROLLER MODE DOESN'T EXIST");
      }

      // Add the PathTarget function
      // Add condition on pathsampler initialization depending on controller_mode_ True False
      // Path Sampler Initialization if needed
      // Follow Path for controller_mode_ 1 & 2
      // Simple PathSampler for controller_mode_ 3

      //X_traj_ = FollowSampledPath(traj_properties_,X_curr_,Xd_curr_);
      Xd_traj_.Zero();
      Xdd_traj_.Zero();*/
	  }
      else
      {
        updateTrajectory(traj_properties_, time_dt);
      }
}

/* double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)
{
    KDL::Frame X_reach, X_curr_r;
    KDL::Vector y_reach;
    //double tol_d_plan = publish_path_sample_size_/2-0.0002;
    double tol_d_plan = 0 ;
    double vel_threshold = 0.000001;

    X_reach = PathFrame(curvilinear_abscissa_tab_[reach_index]);
    y_reach =  KDL::Vector(X_reach(0,1), X_reach(1,1), X_reach(2,1));
    X_curr_r = X_reach.Inverse() * X_curr_; // X_curr_r corresponds to X_curr pose expressed in X_reach frame

    if ((Xd_curr_.vel.y() > vel_threshold) && (X_curr_r.p.y() < -tol_d_plan))
    {
        std::cout << "Case 1" << std::endl;
        // reach_index doesn't change ! // When dot(y_path, y_base) > 0
        // reach_index doesn't change ! // When dot(y_path, y_base) < 0
    }
    //else if ( (dot(Xd_curr_.vel,y_reach) > vel_threshold) && (X_curr_r.p.y() > +tol_d_plan) && (curvilinear_abscissa_tab_[reach_index] <= 1) ) // When dot(y_path, y_base) > 0
    else if ((Xd_curr_.vel.y() > vel_threshold) && (X_curr_r.p.y() > +tol_d_plan) && reach_index > 0) // When dot(y_path, y_base) < 0
    {
        std::cout << "Case 2" << std::endl;
        //reach_index += 1; // When dot(y_path, y_base) > 0
        reach_index -= 1; // When dot(y_path, y_base) < 0
    }
    else if ((Xd_curr_.vel.y() < -vel_threshold) && (X_curr_r.p.y() > +tol_d_plan))
    {
        std::cout << "Case 3" << std::endl;
        // reach_index doesn't change ! // When dot(y_path, y_base) > 0
        // reach_index doesn't change ! // When dot(y_path, y_base) < 0
    }
    //else if (  (dot(Xd_curr_.vel,y_reach) < -vel_threshold) && (X_curr_r.p.y() < -tol_d_plan) && reach_index>0) // When dot(y_path, y_base) > 0
    else if ((Xd_curr_.vel.y() < -vel_threshold) && (X_curr_r.p.y() < -tol_d_plan) && (reach_index < curvilinear_abscissa_tab_.size() - 1)) // When dot(y_path, y_base) < 0
    {
        std::cout << "Case 4" << std::endl;
        //reach_index -= 1; // When dot(y_path, y_base) > 0
        reach_index += 1; // When dot(y_path, y_base) < 0
    }
    else
    {
        std::cout << "Case 5" << std::endl;
        // reach_index doesn't change !
    }

    //std::cout << "reach index = \t" << reach_index  << "\t\tS = " << curvilinear_abscissa_tab_[reach_index] << std::endl;
    X_reach = PathFrame(curvilinear_abscissa_tab_[reach_index]);

    traj_properties_.index_ = reach_index;

    return curvilinear_abscissa_tab_[reach_index]; // TODO Modify it to change frame !
}
 */

double TrajMover::Distance(traj_mover::MoverProperties& mover_properties_ ,KDL::Frame &X_curr_, double &s)
{
    double c_max = 3.5;
    double gamma = PathCurvature(mover_properties_ ,s) / c_max;

    KDL::Frame X_s = PathFrame(mover_properties_,s);
    Eigen::MatrixXd S_effector_control =  Eigen::MatrixXd::Zero(6,6);
    S_effector_control(0,0) = 1;
    S_effector_control(1,1) = 0;
    S_effector_control(2,2) = 1;
    S_effector_control(3,3) = 1;
    S_effector_control(4,4) = 0;
    S_effector_control(5,5) = 1; 
    KDL::Twist X_var =  KDL::diff(PathFrame(mover_properties_ ,s),X_curr_);
    Eigen::Matrix<double, 6,1> x_var;
    tf::twistKDLToEigen(X_var,x_var);
    x_var = S_effector_control * x_var ;
    //std::cout << "x_var = \n" << x_var << std::endl;
    tf::twistEigenToKDL(x_var,X_var);
    double d = mover_properties_.beta * diff(X_s,X_curr_).vel.Norm() + (1 - mover_properties_.beta) * diff(X_s,X_curr_).rot.Norm();
    //double d = (gamma * mover_properties_.beta - gamma + 1) * diff(X_s,X_curr_).vel.Norm() + gamma * (1 - mover_properties_.beta) * diff(X_s,X_curr_).rot.Norm();
    //std::cout << "X_s = " << X_s << std::endl;
    //std::cout << " test= " << mover_properties_.beta << std::endl;
    //std::cout << "gamma = " << gamma << "\t\tPart Angle = " << gamma * (1 - mover_properties_.beta) << std::endl;
    return d;
}

/* double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)
{
    double d_minA, d_minB, d_A, d_B;
    double s_minA, s_minB, s_A, s_B;
    double s_closest;
    double s, d;

    //double N = 1000; 
    double N = mover_properties_.r_sampling; 
    double pas = 10;

    double beta = mover_properties_.beta;

    // Initialization
    s_A = 0;
    s_B = 1;
    d_A = Distance(mover_properties_,X_curr_,s_A);
    d_B = Distance(mover_properties_,X_curr_,s_B);
    
    d_minA = d_A;
    d_minB = d_A;
    s_minA = s_A;
    s_minB = s_A;
    
    for(int q = 10; q <= N; q = q * pas){
        for(int i = 0; i <= pas ; i++){
            s = s_A + double(i) / double(q);
            d = Distance(mover_properties_,X_curr_,s);

            std::cout << "\ns = " << s;

            if(d < d_minA){
                s_minA = s;
                d_minA = d;
            std::cout << "\n\tOK A"  ;
            }
        }
        for(int i = 0; i <= pas ; i++){
            s = s_A + double(i) / double(q);
            d = Distance(mover_properties_,X_curr_,s);
            std::cout << "\ns = " << s;
            if((d < d_minB) && (s!=s_minA)){
                s_minB = s;
                d_minB = d;
            std::cout << "\n\tOK B" ;
            }
        }

        s_closest = s_minA;

        if(s_minB < s_minA){
            s_A = s_minB;
            s_B = s_minA;
            d_A = d_minB;
            d_B = d_minA;
        }
        else{
            s_A = s_minA;
            s_B = s_minB;
            d_A = d_minA;
            d_B = d_minB;
        }
        d_minA = d_A;
        d_minB = d_A;
        s_minA = s_A;
        s_minB = s_A;
    }
    std::cout << "\n\t\tS = " << s_closest << std::endl;

    return s_closest;
}
 */
/* double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)
{
    double s_test, s_closest;
    double d_test, d_closest;
    double s, d;

    //double N = 1000; 
    double N = mover_properties_.r_sampling; 
    double pas = 10;

    double c = PathCurvature(mover_properties_,s_prev);
    double fact = 1;
    mover_properties_.beta = 1 / (fact * c + 1) ; 
    double beta = mover_properties_.beta;
    std::cout << "c = " << c << std::endl;
    std::cout << "beta = " << beta << std::endl;


    // Initialization
    s_test = 0;
    d_test = Distance(mover_properties_,X_curr_,s_test);
    
    for (int i = 0 ; i <= pas ; i++){
        s = i / pas;
        d = Distance(mover_properties_,X_curr_,s); 
        if(d < d_test){
                s_test = s;
                d_test = d;
            }
    }
    s_closest = s_test;
    d_closest = d_test;

    std::cout << "s_MID = " << s_closest << std::endl;
    
    for(int q = pas*pas; q <= N; q = q * pas){
        for(int i = 1; i <= pas ; i++){
            for(int sign = -1 ; sign <= 1 ; sign = sign + 2){
                s = s_closest + sign * double(i) / double(q);
                if ((s >= 0 ) && (s <= 1)){
                    //std::cout << "s_test = " << s << std::endl;
                    d = Distance(mover_properties_,X_curr_,s);
                    if (d < d_test){
                        s_test = s;
                        d_test = d;
                        //std::cout << "Case 1" << std::endl;
                    }
                    else if(d == d_test){
                        s_test = (s_test + s)/2;
                        d_test =  Distance(mover_properties_,X_curr_,s_test);
                        //std::cout << "Case 2" << std::endl;
                    }
                }
            }
        }
        s_closest = s_test;
        d_closest = d_test;
    }
    std::cout << "\n\t\tS = " << s_closest << std::endl;

    KDL::Twist X_var = KDL::diff(PathFrame(mover_properties_ ,s_prev),PathFrame(mover_properties_ ,s_closest));


    
    geometry_msgs::Twist X_var_msg;
    tf::twistKDLToMsg(X_var,X_var_msg);
    mover_properties_.X_var = X_var_msg;
    //double alpha = acos(dot(PathTangentVector(mover_properties_,s_prev), PathTangentVector(mover_properties_,s_closest)));
    X_var.rot(1) = 0;
    double alpha = X_var.rot.Norm();
  
    mover_properties_.alpha = alpha;
    s_prev = s_closest;

    return s_closest;
}
 */
/* double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)
{
    double s_test, s_closest;
    double d_test, d_closest;
    double s, d;

    //double N = 1000; 
    double N = mover_properties_.r_sampling; 
    double pas = 10;
    double beta = mover_properties_.beta;


    // Initialization
    s_test = s_prev;
    d_test = Distance(mover_properties_,X_curr_,s_test);
    s_closest = s_test;
    d_closest = d_test;

    std::cout << "s_MID = " << s_closest << std::endl;
    
    for(int q = pas*pas; q <= N; q = q * pas){
        for(int i = 1; i <= pas ; i++){
            for(int sign = -1 ; sign <= 1 ; sign = sign + 2){
                s = s_closest + sign * double(i) / double(q);
                if ((s >= 0 ) && (s <= 1)){
                    std::cout << "s_test = " << s << std::endl;
                    d = Distance(mover_properties_,X_curr_,s);
                    if (d < d_test){
                        s_test = s;
                        d_test = d;
                        //std::cout << "Case 1" << std::endl;
                    }
                    else if(d == d_test){
                        s_test = (s_test + s)/2;
                        d_test =  Distance(mover_properties_,X_curr_,s_test);
                        //std::cout << "Case 2" << std::endl;
                    }
                }
            }
        }
        s_closest = s_test;
        d_closest = d_test;
    }
    std::cout << "\n\t\tS = " << s_closest << std::endl;

    KDL::Twist X_var = KDL::diff(PathFrame(mover_properties_ ,s_prev),PathFrame(mover_properties_ ,s_closest));


    
    geometry_msgs::Twist X_var_msg;
    tf::twistKDLToMsg(X_var,X_var_msg);
    mover_properties_.X_var = X_var_msg;
    //double alpha = acos(dot(PathTangentVector(mover_properties_,s_prev), PathTangentVector(mover_properties_,s_closest)));
    //X_var.rot(1) = 0;
    double alpha = X_var.rot.Norm();
    double delta = X_var.vel.Norm();
  
    mover_properties_.alpha = alpha;
    mover_properties_.delta = delta;
    s_prev = s_closest;

    return s_closest;
}
 */
double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)
{
    double s_test, s_closest;
    double d_test, d_closest;
    double s, d;

    //double N = 1000; 
    double N = mover_properties_.r_sampling; 
    double pas = 10;
    double beta = mover_properties_.beta;


    // Initialization
    s_test = s_prev;
    d_test = Distance(mover_properties_,X_curr_,s_test);
    s_closest = s_test;
    d_closest = d_test;

    //std::cout << "s_MID = " << s_closest << std::endl;

    KDL::Twist X_var;
    double alpha;
    double delta;

    double alpha_max = 0.8*2e-3;
    double delta_max = 0.8*1.7e-3;

    bool increment;
    
    for(int q = pas*pas; q <= N; q = q * pas){
        int i = 1;
        increment = true;
        while(increment == true){
            for(int sign = -1 ; sign <= 1 ; sign = sign + 2){
                s = s_closest + sign * double(i) / double(q);
                X_var = KDL::diff(PathFrame(mover_properties_ ,s_prev),PathFrame(mover_properties_ ,s));
                alpha = X_var.rot.Norm();
                delta = X_var.vel.Norm();
                increment = false;
                if(alpha < alpha_max && delta < delta_max){
                    increment = true;
                    if ((s >= 0 ) && (s <= 1)){
                        //std::cout << "s_test = " << s << std::endl;
                        d = Distance(mover_properties_,X_curr_,s);
                        if (d < d_test){
                            s_test = s;
                            d_test = d;
                            //std::cout << "Case 1" << std::endl;
                        }
                        else if(d == d_test){
                            s_test = (s_test + s)/2;
                            d_test =  Distance(mover_properties_,X_curr_,s_test);
                            //std::cout << "Case 2" << std::endl;
                        }
                    }
                }
            }
            i++;
        }
        s_closest = s_test;
        d_closest = d_test;
    }
    //std::cout << "\n\t\tS = " << s_closest << std::endl;

    X_var = KDL::diff(PathFrame(mover_properties_ ,s_prev),PathFrame(mover_properties_ ,s_closest));


    
    geometry_msgs::Twist X_var_msg;
    tf::twistKDLToMsg(X_var,X_var_msg);
    mover_properties_.X_var = X_var_msg;
    alpha = X_var.rot.Norm();
    delta = X_var.vel.Norm();
    double curv = PathCurvature(mover_properties_ ,s_prev);
  
    mover_properties_.alpha = alpha;
    mover_properties_.delta = delta;
    mover_properties_.curv = curv;
    s_prev = s_closest;

    return s_closest;
}


KDL::Vector TrajMover::PathTangentVector(traj_mover::MoverProperties& mover_properties_ ,double s)
{ // Corresponds to derivative of the path
    KDL::Vector T = PathDerivative(mover_properties_,s);
    T = T / T.Norm(); // Normalization
    return T;
}

KDL::Vector TrajMover::PathNormalVector(traj_mover::MoverProperties& mover_properties_ ,double s)
{ // Corresponds to derivative of the path
    KDL::Vector N = PathDoubleDerivative(mover_properties_,s);
    N = N / N.Norm(); // Normalization
    return N;
}

KDL::Frame TrajMover::PathFrame(traj_mover::MoverProperties& mover_properties_ ,double s)
{
    // Variables
    double r11, r12, r13, r21, r22, r23, r31, r32, r33;
    KDL::Rotation R;
    KDL::Vector T;
    KDL::Frame X_path_;

    //// Compute Translation Vector
    X_path_.p = PathFunction(mover_properties_,s);

    //// Compute Rotation Matrix
    // Tangent vector
    T = PathTangentVector(mover_properties_,s);
    r12 = -T.x();
    r22 = -T.y();
    r32 = -T.z();

    // Normal Vector

    r13 = 0; // on the plane Oy_0z_0, orthogonal to x_0 vector
    if (r32 == 0)
    {
        r23 = 0;
        r33 = 1;
    }
    else
    {
        r23 = sqrt(1 / (pow((r22 / r32), 2) + 1));
        r33 = -r23 * r22 / r32;
    }
    if (r33 > 0) // we want it always negative
    {
        r13 = -r13;
        r23 = -r23;
        r33 = -r33;
    }

    
    // 2nd Normal Vector, orthogonal to Tangent and Normal vectors
    r11 = r22 * r33 - r32 * r23;
    r21 = r32 * r13 - r12 * r33;
    r31 = r12 * r23 - r22 * r13;

    R = KDL::Rotation(r11, r21, r31, r12, r22, r32, r13, r23, r33); //Tangent vector is Y axis of the effector, Normal vector is Z axis
    X_path_.M = R.Inverse();                                        // R corresponds to V_traj=R(traj-0) V_0, but the code needs the R(0-traj) matrix !

    KDL::Rotation R_offset = KDL::Rotation::RPY(0,0,0);
    KDL::Frame F_offset(R_offset);
    return X_path_ * F_offset;
}

KDL::Vector TrajMover::PathFunction(traj_mover::MoverProperties& mover_properties_ ,double s)
{
    // Variables
    double x, y, z;

    switch(mover_properties_.path_no){
        case 1:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = 0.3*sin(M_PI*s) + 0.2;
            break;
        case 2:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = -3.13841851841079e-7*s + 0.0652173913043478*pow(cos(2*M_PI*s),2) + 0.0489130434782609*cos(6*M_PI*s) + 0.285869565217391;
            break;
        case 3:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = 0.130434782608696*pow(cos(2*M_PI*s),2) + 0.0978260869565217*cos(6*M_PI*s) - 0.0282608695652174 ;
            break;
        case 4:
            x = 0.5;
            y = 0.6*s - 0.3;
            z =  0.130434782608696*pow(cos(2*M_PI*s),2) + 0.0978260869565217*cos(6*M_PI*s) + 0.171739130434783;
            break;
        case 5:
            x = 0.5;
            y = 0.4*s - 0.1;
            z = 0.15*sin(M_PI*s) + 0.4;
            break;
        case 6:
            x = 0.5;
            y = 0.6 * s - 0.2;
            z =  0.0652173913043478*pow(cos(2*M_PI*s),2) + 0.0489130434782609*cos(6*M_PI*s) + 0.285869565217391 ;
            break;
        case 7:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = -0.05*atan(atan(cos(3*M_PI*s/2))) + 0.075*atan(atan(cos(4*M_PI*s))) - 0.025*atan(M_PI/4) + 0.4;
            break;
        case 8:
            x = 0.5;
            y = 0.6 * s - 0.2;
            z = 0.15*s + 0.4 ;
            break;
        case 9:
            x = 0.5;
            y = 0.6 * s - 0.2;
            z = 0.15*s + 0.5 ;
            break;
        case 10:
            x = 0.5;
            y = 0.6 * s - 0.2;
            z =  0.65 - 0.6*pow(s - 0.5,2) ;
            break;
        case 11:
            x = 0.5;
            y = 0.6 * s - 0.2;
            z = 0.075 * sin(3 * M_PI * s) + 0.5;
            break;
        case 12:
            x = 0.4;
            y = 0.7 * s - 0.3;
            z = -0.2*s + 0.65 ;
            break;
        case 13:
            x = 0.5;
            y = 0.4 * s - 0.2;
            z = 0.05 * sin(3 * M_PI * s) + 0.5;
            break;
        case 14:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = 0.65 - 0.6*pow(s - 0.5,2);
            break;
        case 15:
            x = 0.5;
            y = 0.4*s - 0.2;
            z = 0.35 - 0.6*pow(s - 0.5,2);
            break;
    }
/*
    // Define functions of s --> computed from a python script (Calcul Trajectoire Polynomiale-v3 on 15/04/2021)
    x = 0.5; // Trials 1,2,3,4,5,6,7,8,9,10,11
    //x = 0.4; // Trial 12

    //y = 0.4*s - 0.2;  // Trials 1,2,3,7 // WARNING ! DON'T USE WITH HANDLE !
    //y = 0.6*s - 0.3;    // Trials 4 // WARNING ! DON'T USE WITH HANDLE !
    //y = 0.4*s - 0.1; // Trials 5 // WARNING ! DON'T USE WITH HANDLE !
    y = 0.6 * s - 0.2; // Trial 6,8,9,10,11
    //y = 0.7 * s - 0.3; // Trial 12

    //z = 0.3*sin(M_PI*s) + 0.2; // Trials 1
    //z = -3.13841851841079e-7*s + 0.0652173913043478*pow(cos(2*M_PI*s),2) + 0.0489130434782609*cos(6*M_PI*s) + 0.285869565217391; // Trials 2 // WARNING ! DON'T USE WITH HANDLE !
    //z = 0.130434782608696*pow(cos(2*M_PI*s),2) + 0.0978260869565217*cos(6*M_PI*s) - 0.0282608695652174 ; //// Trials 3 // WARNING ! DON'T USE WITH HANDLE !
    //z =  0.130434782608696*pow(cos(2*M_PI*s),2) + 0.0978260869565217*cos(6*M_PI*s) + 0.171739130434783; // Trials 4 // WARNING ! DON'T USE WITH HANDLE !
    //z = 0.15*sin(M_PI*s) + 0.4; // Trials 5 // WARNING ! DON'T USE WITH HANDLE !
    //z =  0.0652173913043478*pow(cos(2*M_PI*s),2) + 0.0489130434782609*cos(6*M_PI*s) + 0.285869565217391 ; // Trial 6 // WARNING ! DON'T USE WITH HANDLE !
    //z = -0.05*atan(atan(cos(3*M_PI*s/2))) + 0.075*atan(atan(cos(4*M_PI*s))) - 0.025*atan(M_PI/4) + 0.4; // Trial 7 // WARNING ! DON'T USE WITH HANDLE !
    //z = 0.15*s + 0.4 ; // Trial 8 // WARNING ! DON'T USE WITH HANDLE !
    //z = 0.15*s + 0.5 ; // Trial 9
    z =  0.65 - 0.6*pow(s - 0.5,2) ; // Trial 10
    //z = 0.075 * sin(3 * M_PI * s) + 0.5; // Trial 11
    //z = -0.2*s + 0.65 ; // Trial 12
    //z = 0.5 ; // Trial TEST LUCAS

*/
    return KDL::Vector(x, y, z) + oProot_kdl; // add offset from world frame to root robot frame
}

KDL::Vector TrajMover::PathDerivative(traj_mover::MoverProperties& mover_properties_ ,double s)
{ // Corresponds to derivative of the path
    // Variables
    double dx, dy, dz, norm;
    KDL::Vector dX;
    switch(mover_properties_.path_no){
        case 1:
            dx = 0;
            dy = 0.4;
            dz = 0.3*M_PI*cos(M_PI*s);
            break;
        case 2:
            dx = 0;
            dy = 0.4;
            dz = -0.260869565217391*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.293478260869565*M_PI*sin(6*M_PI*s) - 3.13841851841079e-7;
            break;
        case 3:
            dx = 0;
            dy = 0.4;
            dz =  -0.521739130434783*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.58695652173913*M_PI*sin(6*M_PI*s); 
            break;
        case 4:
            dx = 0;
            dy = 0.6;
            dz = -0.521739130434783*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.58695652173913*M_PI*sin(6*M_PI*s) ; 
            break;
        case 5:
            dx = 0;
            dy = 0.4;
            dz = 0.15*M_PI*cos(M_PI*s) ; 
            break;
        case 6:
            dx = 0;
            dy = 0.6;
            dz = -0.260869565217391*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.293478260869565*M_PI*sin(6*M_PI*s) ;
            break;
        case 7:
            dx = 0;
            dy = 0.4;
            dz = -0.3*M_PI*sin(4*M_PI*s)/((pow(cos(4*M_PI*s),2) + 1)*(pow(atan(cos(4*M_PI*s)),2) + 1)) + 0.075*M_PI*sin(3*M_PI*s/2)/((pow(cos(3*M_PI*s/2),2) + 1)*((atan(cos(3*M_PI*s/2)),2) + 1));
            break;
        case 8:
            dx = 0;
            dy = 0.6;
            dz = 0.15 ;
            break;
        case 9:
            dx = 0;
            dy = 0.6;
            dz = 0.15 ;
            break;
        case 10:
            dx = 0;
            dy = 0.6;
            dz = 0.6 - 1.2 * s ; 
            break;
        case 11:
            dx = 0;
            dy = 0.6;
            dz = 0.225 * M_PI * cos(3 * M_PI * s);
            break;
        case 12:
            dx = 0;
            dy = 0.7;
            dz = -0.2;
            break;
        case 13:
            dx = 0;
            dy = 0.4;
            dz = 0.225 * M_PI * cos(3 * M_PI * s);
            break;
        case 14:
            dx = 0;
            dy = 0.4;
            dz = 0.6 - 1.2*s;
            break;
        case 15:
            dx = 0;
            dy = 0.4;
            dz = 0.6 - 1.2*s;
            break;
    }
/*
    // Define functions of s --> computed from a python script (Calcul Trajectoire Polynomiale-v3 on 15/04/2021)
    dx = 0; // Trials 1,2,3,4,5,6,7,8, 9,10,11,12

    //dy = 0.4;         // Trials 1,2,3,5,7
    dy = 0.6; // Trials 4,6,8, 9,10,11
    //dy = 0.7; // Trials 12

    //dz = 0.3*M_PI*cos(M_PI*s); // Trials 1
    //dz = -0.260869565217391*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.293478260869565*M_PI*sin(6*M_PI*s) - 3.13841851841079e-7; // Trials 2
    //dz =  -0.521739130434783*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.58695652173913*M_PI*sin(6*M_PI*s); // Trials 3
    //dz = -0.521739130434783*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.58695652173913*M_PI*sin(6*M_PI*s) ; // Trials 4
    //dz = 0.15*M_PI*cos(M_PI*s) ; // Trials 5
    //dz = -0.260869565217391*M_PI*sin(2*M_PI*s)*cos(2*M_PI*s) - 0.293478260869565*M_PI*sin(6*M_PI*s) ; // Trial 6
    //dz = -0.3*M_PI*sin(4*M_PI*s)/((pow(cos(4*M_PI*s),2) + 1)*(pow(atan(cos(4*M_PI*s)),2) + 1)) + 0.075*M_PI*sin(3*M_PI*s/2)/((pow(cos(3*M_PI*s/2),2) + 1)*((atan(cos(3*M_PI*s/2)),2) + 1)); // Trial 7
    //dz = 0.15 ; // Trial 8, 9
    dz = 0.6 - 1.2 * s ; // Trial 10
    //dz = 0.225 * M_PI * cos(3 * M_PI * s); // Trial 11
    //dz = -0.2; // Trial 12
    //dz = 0; // Trial LUCAS

    //dz = 0.0652173913043478; // test straight line
*/
    dX = KDL::Vector(dx, dy, dz);
    return dX;
}

KDL::Vector TrajMover::PathDoubleDerivative(traj_mover::MoverProperties& mover_properties_ ,double s)
{ // Corresponds to double derivative of the path
    // Variables
    double ddx, ddy, ddz;
    KDL::Vector ddX;

    switch(mover_properties_.path_no){
        case 1:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 2:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 3:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 4:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 5:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 6:
            ddx = 0;
            ddy = 0;
            ddz =  0.521739130434783*M_PI*M_PI*pow(sin(2*M_PI*s),2) - 0.521739130434783*M_PI*M_PI*pow(cos(2*M_PI*s),2) - 1.76086956521739*M_PI*M_PI*cos(6*M_PI*s) ; 
            break;
        case 7:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 8:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 9:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 10:
            ddx = 0;
            ddy = 0;
            ddz = -1.2;
            break;
        case 11:
            ddx = 0;
            ddy = 0;
            ddz = -0.675 * M_PI * M_PI * sin(3 * M_PI * s);
            break;
        case 12:
            ddx = 0;
            ddy = 0;
            ddz = 0;
            break;
        case 13:
            ddx = 0;
            ddy = 0;
            ddz = -0.675 * M_PI * M_PI * sin(3 * M_PI * s);
            break;
        case 14:
            ddx = 0;
            ddy = 0;
            ddz = -1.2;
            break;
        case 15:
            ddx = 0;
            ddy = 0;
            ddz = -1.2;
            break;
    }

    // Define functions of s --> computed from a python script (Calcul Trajectoire Polynomiale-v3 on 15/04/2021)
    ddx = 0; // Trials 1,2,3,4,5,6,7,9,10,11,12

    ddy = 0; // Trials 1,2,3,4,5,6,7,8,9,10,11,12

    //ddz =  0.521739130434783*M_PI*M_PI*pow(sin(2*M_PI*s),2) - 0.521739130434783*M_PI*M_PI*pow(cos(2*M_PI*s),2) - 1.76086956521739*M_PI*M_PI*cos(6*M_PI*s) ; // Trials 6
    //ddz = 0 ; // Trial 8, 9,12
    ddz = -1.2; // Trial 10
    //ddz = -0.675 * M_PI * M_PI * sin(3 * M_PI * s); // Trial 11

    //ddz = 0 ; // test Lucas

    ddX = KDL::Vector(ddx, ddy, ddz);
    return ddX;
}



double TrajMover::PathCurvature(traj_mover::MoverProperties& mover_properties_ ,double &s)
{
    KDL::Vector Xd = PathDerivative(mover_properties_,s);
    KDL::Vector Xdd = PathDoubleDerivative(mover_properties_,s);
    double c = sqrt((pow(Xd.Norm(), 2) * pow(Xdd.Norm(), 2) - pow(dot(Xd, Xdd), 2)) / pow(Xd.Norm(), 6));
    return c;
}

void TrajMover::PathPreSamplerInit(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_,double k, double dt)
{
    int i = 0;
    double s_ini = 0;
    double s_next;
    while (s_ini <= 1) // use custom pitch for controlling (by using curvilinear_abscissa_tab_) ==> this pitch is used in FollowPath function
    {
        //std::cout << "s= " << s_ini << std::endl;
        if (s_ini == 0)
        {
            curvilinear_abscissa_tab_.clear();
        }
        curvilinear_abscissa_tab_.push_back(s_ini);
        s_next = PathSampler(traj_properties_,mover_properties_, s_ini, k, dt);
        s_ini = s_next;
    }
    presampler_initialized_ = true;
}

double TrajMover::PathSampler(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_ , double s_curr, double k, double dt)
{
    // Robot Limits
    double d_max = 1.4e-3;   // 1.7e-3 nominal limit
    double alpha_max = 2e-3; //2.5e-3 nominal limit

    double delta_l_next;
    // TODO put delta_s in parameters
    //double delta_s = traj_properties_.delta_s_;
    double delta_s = 0.001;

    // Constant Sampling
    if (k == 0)
    {
        delta_l_next = d_max / 20;
    }
    // Power Law Sampling
    else
    {
        double c = PathCurvature(mover_properties_,s_curr);
        double V = k / pow(c, (1 / 3.0));
        delta_l_next = V * dt;
        delta_l_next = std::min(delta_l_next, d_max); // Verify if distance is to large for the robot
    }

    //Knowing s_curr, we want s_{i+1}
    double s_next = s_curr + delta_s;
    double delta_l = (PathFunction(mover_properties_,s_next) - PathFunction(mover_properties_,s_curr)).Norm();
    while (delta_l < delta_l_next)
    {
        delta_l = delta_l + (PathFunction(mover_properties_,s_next + delta_s) - PathFunction(mover_properties_,s_next)).Norm();
        s_next = s_next + delta_s;
    }
    
    
    // Compute angle between new tangent and previous tangent
    double alpha = acos(dot(PathTangentVector(mover_properties_,s_curr), PathTangentVector(mover_properties_,s_next)));

    //Verify robot ability to perform the rotation from previous tangent and new tangent
    if (alpha > alpha_max)
    {
        //ROS_WARN_STREAM("Important curvature: Impossible to sample");
        
    }

    //cout << "s_curr = " << s_curr << "\ts_next = " << s_next  << endl;
    return s_next;
}
/*

double TrajMover::OnlineSamplingPath(kdl_trajectories::TrajProperties &traj_properties_, KDL::Frame &X_curr, KDL::Twist &Xd_curr, double &time_dt)
{
    //ROS_WARN_STREAM("NO IMPLEMENTATION CURRENTLY");

    //cout << "s_trajprop = " << traj_properties_.s_next_ << endl;
    //iteratively find si* with T_curr and T(s): FIND CURRENT REFERENCE ABSCISSA s_curr
    //double s_iter = ArgMinDistFrame(traj_properties_,X_curr);
    //cout << "s_iter = " << s_iter << endl;
    //double s_raphson = PathClosestFrame(traj_properties_, X_curr,traj_properties_.s_next_);
    double s_curr = traj_properties_.s_next_;
    s_curr = PathClosestAbscissa(traj_properties_, X_curr, s_curr);
    //cout << "s_raphson = " << s_raphson << endl;

    // ########################################################
    // HARDCODING V HUMAN MAX
    // ########################################################
    double v_h_max = 1; // m/s

    // Compute c_i with si*
    double c_curr = PathCurvature(s_curr);

    //compute k_i with c_i and V_curr
    double k_i = Xd_curr.vel.Norm() * pow(c_curr, 1.0 / 3.0);

    // Compute k_mean from k_i and previous k
    double k_23PL_tab_maxsize = 10000; // SIZE OF THE SLIDING WINDOW

    k_23PL_tab_.push_back(k_i);
    if (k_23PL_tab_.size() > k_23PL_tab_maxsize)
    {
        k_23PL_tab_.pop_front();
    }
    double k_mean = 0;
    for (double k : k_23PL_tab_)
    {
        k_mean = k_mean + k;
    }
    k_mean = k_mean / k_23PL_tab_.size();
    cout << "k_i = " << k_i << "\t\t//\tk_mean = " << k_mean << "\t\t//\tN = " << k_23PL_tab_.size() << endl;

    // Compute Vi+1 twist from c_i and k_mean
    KDL::Vector Omega_next = Xd_curr.rot / Xd_curr.rot.Norm() * k_mean * pow(c_curr, 2.0 / 3.0);
    double v_next_norm;
    if (c_curr != 0)
    {
        v_next_norm = min(v_h_max, k_mean * pow(1 / c_curr, 1.0 / 3.0));
    }
    else
    {
        v_next_norm = v_h_max;
    }
    KDL::Vector V_next = Xd_curr.vel / Xd_curr.vel.Norm() * v_next_norm;

    if (V_next.Norm() > 1)
    {
        ROS_ERROR_STREAM("####################\nFAST\n#################");
    }

    KDL::Twist Xd_des = KDL::Twist(V_next, Omega_next);

    //Compute Ti+1_des from Vi+1 twist and T_curr
    KDL::Frame X_des = ExpCoordinates(Xd_des, time_dt) * X_curr; // use the exponential expression of Xd_des

    //iteratively find si+1* with Ti+1_des and T(s)
    //double s_des_raphson = PathClosestFrame(traj_properties_, X_des,traj_properties_.s_next_);
    double s_des = PathClosestAbscissa(traj_properties_, X_des, s_curr);
    //double s_des_iter = ArgMinDistFrame(traj_properties_,X_des);
    //double s_des= s_des_raphson;
    if (abs(s_des - traj_properties_.s_next_) > 0.01)
    {
        ROS_ERROR_STREAM("####################\nJUMP\n#################");
    }

    return s_des;
}

double TrajMover::ArgMinDistFrame(kdl_trajectories::TrajProperties &traj_properties_, KDL::Frame &X_des)
{
    double rho = 0.5; // Weight between rotation part and translation part : rho = 1 is fully translated, rho = 0 is fully rotated
    double d_min, d_s;
    double s_min = 0, s = 0;
    KDL::Frame X_s = PathFrame(s);
    d_min = DistFrames(traj_properties_, X_des, X_s, rho);
    for (int i = 0; i < int(1 / traj_properties_.delta_s_); i++)
    {
        s = i * traj_properties_.delta_s_;
        X_s = PathFrame(s);
        d_s = DistFrames(traj_properties_, X_des, X_s, rho);
        if (d_s < d_min)
        { // find the closest frame on the path from X_des, considering DistFrame function
            d_min = d_s;
            s_min = s;
        }
    }
    return s_min;
}
*/

kdl_trajectories::PublishTraj TrajMover::publishTrajectoryMover(traj_mover::MoverProperties& mover_properties_)
{
    kdl_trajectories::PublishTraj publish_traj_;
    // publish trajectory
    nav_msgs::Path path_ros;
    geometry_msgs::PoseArray pose_array;
    geometry_msgs::PoseStamped pose_st;

    ROS_WARN_STREAM("PUBLISHING TRAJECTORY");
    if (mover_properties_.follower_ && mover_properties_.follow_init_)
    {
        ROS_WARN_STREAM("FOLLOWER MODE");
        for (double s = 0 ; s < 1 ; s+= 0.001) // use constant pitch for publishing
        {
        KDL::Frame current_pose;
        current_pose = PathFrame(mover_properties_,s);
        geometry_msgs::Pose pose;
        tf::poseKDLToMsg(current_pose, pose);
        pose_array.poses.push_back(pose);
        pose_st.pose = pose;
        path_ros.poses.push_back(pose_st);
        }
        // PathPreSamplerInit function calling

        std::cout << "CONTROLLING POSE ARRAY SIZE= " << curvilinear_abscissa_tab_.size() << std::endl;
        mover_properties_.nb_samples_ = curvilinear_abscissa_tab_.size();
        //for (int i=0; i<  curvilinear_abscissa_tab_.size();++i)
        //    std::cout << curvilinear_abscissa_tab_.data()[i] << endl;

        publish_traj_.pose_array_ = pose_array;
        publish_traj_.path_ros_ = path_ros;

        return publish_traj_;
    }
    else
    {
        std::cout << "movement_list\n" << movement_list[0].frame << std::endl;
        ROS_WARN_STREAM("GENERIC MODE");
        return publishTrajectory();
    }
}

